# Pull from home assistant
FROM homeassistant/amd64-homeassistant

# I'm the one who did this!
MAINTAINER Curtis Holland <curtis.holland@gmail.com>

# Create an empty automations file
RUN touch automations.yaml

# Copy all of the yaml files to the correct location
COPY secrets.example.yaml secrets.yaml
COPY *.yaml .
