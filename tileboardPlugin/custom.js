let switchButton = (position, id, title, subtitle) => {
    let objReturn = {
        position, id, title, subtitle,
        type: TYPES.SWITCH,
        states: {
            on: "On",
            off: "Off"
        },
        icons: {
            on: "mdi-lightbulb-on",
            off: "mdi-lightbulb",
        }
    };
    return objReturn;
};

let lightButton = (position, id, brightnessSlider, colorTemp, title, subtitle) => {
    let objReturn = {
        position, title, subtitle, id,
        type: TYPES.LIGHT,
        states: {
            on: "On",
            off: "Off"
        },
        icons: {
            on: "mdi-lightbulb-on",
            off: "mdi-lightbulb",
        },
        sliders: []
    }
    if(brightnessSlider){
        objReturn.sliders.push({
            title: 'Brightness',
            field: 'brightness',
            max: 255,
            min: 0,
            step: 5,
            request: {
                type: "call_service",
                domain: "light",
                service: "turn_on",
                field: "brightness"
            }
        });
    }
    if(colorTemp){
        objReturn.sliders.push({
            title: 'Color temp',
            field: 'color_temp',
            max: 588,
            min: 153,
            step: 15,
            request: {
                type: "call_service",
                domain: "light",
                service: "turn_on",
                field: "color_temp"
            }
        });
    }
    return objReturn;
};

let weatherMappingIcons = {
    // these map to weather underground
    "cloudy": 'cloudy',
    "fog": 'fog',
    "hail": 'sleet',
    "lightning": 'chancetstorms',
    "lightning-rainy": 'tstorms',
    "partlycloudy": 'partlycloudy',
    "pouring": 'rain',
    "rainy": 'rain',
    "snowy": 'snow',
    "snowy-rainy": 'chancesleet',
    "sunny": 'clear',
    "windy": 'fog',
    "windy-variant": 'fog',
    "exceptional": 'clear'
};

let doorValue = function(state){
    switch(state){
        case 'off':
            return "closed";
        case 'on':
            return "open";
    }
}

let batteryIconFunction = function(percent){
    if(percent >= 91){
        return 'mdi-battery';
    }else if(percent <= 90 && percent >= 81){
        return 'mdi-battery-80';
    }else if(percent <= 80 && percent >= 71){
        return 'mdi-battery-70';
    }else if(percent <= 70 && percent >= 61){
        return 'mdi-battery-60';
    }else if(percent <= 60 && percent >= 51){
        return 'mdi-battery-50';
    }else if(percent <= 50 && percent >= 41){
        return 'mdi-battery-40';
    }else if(percent <= 40 && percent >= 31){
        return 'mdi-battery-30';
    }else if(percent <= 30 && percent >= 21){
        return 'mdi-battery-20';
    }else if(percent <= 20 && percent >= 11){
        return 'mdi-battery-10';
    }else if(percent <= 10 && percent >= 1){
        return 'mdi-battery-outline';
    }

};