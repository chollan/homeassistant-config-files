# Tileboard Plugin Config Files

Tileboard can be found here: https://github.com/resoai/TileBoard

## Installation

    mkdir <ha-config>/www/tileboard
    cd <ha-config>/www/tileboard
    git clone https://github.com/resoai/TileBoard
    yarn    #unconfirmed.  make sure yarn installs what it needs.
    npm run-scripts build

## Further Configuration

This directory contains configuration files to be plaed in the tileboard build directory created above.

Directory Structure:

* `config.js` will be copied to `<ha config>/www/tileboard/build/`
* `custom.css` will be copied to `<ha config>/www/tileboard/build/styles/`
* `custom.js` will be copied to `<ha config>/www/tileboard/build/`
* `voice/` will be copied to `<ha config>/www/tileboard/build/voice/`

## `custom.js` Setup

The config.js file loads the configuration for the tilebord.  
The configuration file contains many funcion calls that make configuration easier.  
These functions are defined within `custom.js`.

Support for the `custom.js` file doesn't exist out of the box.  we will need to add it to the `<ha config>/www/tileboard/build/index.html` file.

Add the following line at the end of the `<head>` section:

    <script src="./scripts/custom.js"></script>

## Voice files

Were created by AWS Poly (https://console.aws.amazon.com/polly/home/SynthesizeSpeech) using the Joey, Male voice 