#!/bin/bash
CONTAINER=`docker ps |grep home\-assistant |cut -f1 -d' '`
docker exec -it $CONTAINER python -m homeassistant --script check_config --config /config