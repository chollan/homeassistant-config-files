#!/bin/bash

docker run -it --rm -v ${PWD}/:/config --entrypoint /usr/local/bin/python homeassistant/home-assistant:latest -m homeassistant --script check_config --config /config