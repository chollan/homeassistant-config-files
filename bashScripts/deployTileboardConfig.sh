#!/bin/bash

scp tileboardPlugin/config.js nas:/apps/docker/home-assistant/config/www/tileboard/build/config.js
scp tileboardPlugin/custom.css nas:/apps/docker/home-assistant/config/www/tileboard/build/styles/custom.css
scp tileboardPlugin/custom.js nas:/apps/docker/home-assistant/config/www/tileboard/build/scripts/custom.js

# Uncomment the below if we want to copy over the voice directory
#rsync -avh --progress tileboardPlugin/voice nas:/apps/docker/home-assistant/config/www/tileboard/build/