# My Home Automation

Remotec ZCR 90: https://www.home-assistant.io/docs/z-wave/device-specific/#remotec-zrc-90-scene-master
convert audio to mp3: ffmpeg -i doorbell.mp3 -ac 2 -codec:a libmp3lame -b:a 48k -ar 16000 doorbell-2.mp3


chime_tts demo
action: chime_tts.say
data:
  entity_id: 
    - media_player.bedroom_echo_dot
    - media_player.office
    - media_player.living_room_echo_dot
    - media_player.basement_echo_dot
    - media_player.kitchen_echo_pop
    - media_player.curtis_s_echo_studio
  chime_path: custom_components/chime_tts/mp3s/chord.mp3
  # end_chime_path: custom_components/chime_tts/mp3s/tada.mp3
  offset: 650
  final_delay: 100
  message: "Ive found a new tool to make announcements thruout the house"
  tts_platform: amazon_polly
  # tts_speed: 125
  # tts_pitch: 3
  volume_level: 1
  # join_players: true
  # unjoin_players: true
  cache: true
  announce: true
  fade_audio: true
  audio_conversion: Alexa
  # language: en
  # tld: com.au
  voice: Joanna
  # options: "tld: com.au\voice: en-AU"
